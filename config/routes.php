<?php
/**
 * Created by PhpStorm.
 * User: $WebDev
 * Date: 14.05.2020
 * Time: 2:59
 */
$routes = [];

$routes["default"] = "bin\\controllers\\mainController|index";
$routes["@"] = "bin\\controllers\\dashboardController|index";
$routes["@/auth"] = "bin\\controllers\\dashboardController|dbAuthorisation";

$routes["cart"] = "bin\\controllers\\cartController|index";
$routes["login"] = "bin\\controllers\\userController|login";
$routes["products/views/.*"] = "bin\\controllers\\productsController|product_card";
$routes["product/category"] = "bin\\controllers\\productsController|product_cat";
$routes["favorites"] = "bin\\controllers\\userController|favorites";
$routes["logout"] = "bin\\controllers\\userController|logout";


$routes["login/active"] = "bin\\controllers\\userController|active";
$routes["login/history"] = "bin\\controllers\\userController|history";
$routes["login/constructor"] = "bin\\controllers\\userController|constructor";
$routes["login/profile"] = "bin\\controllers\\userController|profile";



$routes["about"] = "bin\\controllers\\staticController|about";
$routes["news"] = "bin\\controllers\\staticController|news";
$routes["contacts"] = "bin\\controllers\\staticController|contacts";
$routes["sales"] = "bin\\controllers\\staticController|sales";
$routes["delivery"] = "bin\\controllers\\staticController|delivery";
$routes["forclients"] = "bin\\controllers\\staticController|forClients";
$routes["vacancy"] = "bin\\controllers\\staticController|vacancy";
