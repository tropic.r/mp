<?php
/**
 * Created by PhpStorm.
 * User: $WebDev
 * Date: 14.05.2020
 * Time: 3:14
 */

namespace bin\controllers;

use \bin\main\container;

class staticController extends container
{
    public function about()
    {
        return $this->twig()->render("front/statics/about.twig");
    }

    public function news()
    {
        return $this->twig()->render("front/statics/news.twig");
    }


    public function contacts()
    {
        return $this->twig()->render("front/statics/contacts.twig");
    }

    public function sales()
    {
        return $this->twig()->render("front/statics/sales.twig");
    }


    public function delivery()
    {
        return $this->twig()->render("front/statics/delivery.twig");
    }

    public function forClients()
    {
        return $this->twig()->render("front/statics/forClients.twig");
    }

    public function vacancy()
    {
        return $this->twig()->render("front/statics/vacancy.twig");

    }
}