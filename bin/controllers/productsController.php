<?php
/**
 * Created by PhpStorm.
 * User: $WebDev
 * Date: 14.05.2020
 * Time: 3:14
 */

namespace bin\controllers;

use \bin\main\container;

class productsController extends container
{
    public function product_card()
    {
        return $this->twig()->render("front/product_card.twig");
    }


    public function product_cat()
    {
        return $this->twig()->render("front/product_cat.twig");
    }
}