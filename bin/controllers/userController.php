<?php
/**
 * Created by PhpStorm.
 * User: $WebDev
 * Date: 14.05.2020
 * Time: 3:14
 */

namespace bin\controllers;

use \bin\main\container;
use \RedBeanPHP\R;
use \helpers\redirect;
use \helpers\message;
use \helpers\userSession;

class userController extends container
{
    public function active()
    {
        return $this->twig()->render("front/cabinet/active.twig");
    }


    public function history()
    {
        return $this->twig()->render("front/cabinet/history.twig");
    }


    public function constructor()
    {
        return $this->twig()->render("front/cabinet/constructor.twig");
    }


    public function profile()
    {
        return $this->twig()->render("front/cabinet/profile.twig");
    }




    public function logout()
    {
        setcookie("u_email",false,time()-14400,"/");
        setcookie("u_tel",false,time()-14400,"/");
        setcookie("u_in",false,time()-14400,"/");
        unset($_SESSION['user_ip']);
        unset($_SESSION['user_agent']);
        redirect::userlogin();
    }


    public function login()
    {
        if(isset($_POST['u_reg'])) {
            $activate_number = md5(uniqid());
            $array = R::getRow("SELECT email FROM users WHERE email IN(?)", [trim($_POST['u_email'])]);
            if(empty($array['email'])){

                //user
                $user = R::dispense('users');
                $user->email = trim($_POST['u_email']);
                $user->pass = password_hash(trim($_POST['u_password']), PASSWORD_DEFAULT);
                $user->ban = 0;
                $user->activate = 1;
                $user->activate_num = $activate_number;
                $user->usergroup_id = 1;
                $userId = R::store($user);
                if ($userId > 0) {
                    //phone
                    $phones = R::dispense('userphones');
                    $phones->user_id = $userId;
                    $phones->phone = $_POST['u_tel'];
                    $phoneId = R::store($phones);


                    //info
                    $info = R::dispense('userinfo');
                    $info->user_id = $userId;
                    $info->company_name = $_POST['u_company'];
                    $info->city_id = $_POST['u_city'];
                    $infoId = R::store($info);

                    if ($phoneId > 0 && $infoId > 0) {
    //                    redirect::reload();
                        message::set("Регистрация прошла успешно");
                    } else {
                        message::set("Ошибка регистрации");
                    }

                } else {
                    message::set("Ошибка регистрации");
                }
            }else{
                message::set("Такой пользователь уже существует!!!");
            }
        }


        if(isset($_POST['u_enter'])){
            if(!empty(trim($_POST['u_email'])) && !empty(trim($_POST['u_password']))){
               $userIn =  R::getRow("SELECT u.*,phone,i.company_name,c.city FROM users u 
                                        LEFT JOIN userphones ph ON u.id=ph.user_id
                                        LEFT JOIN userinfo i ON i.user_id=u.id
                                        LEFT JOIN cities c ON i.city_id=c.id
                                        WHERE u.email IN(?)
                                        ", [trim($_POST['u_email'])]);

               if(!empty($userIn)){
                   if(password_verify(trim($_POST['u_password']),$userIn['pass'])){
                       userSession::setIp();

                       setcookie("u_email",$userIn['email'],time()+14400,"/");
                       setcookie("u_tel",$userIn['phone'],time()+14400,"/");
                       setcookie("u_in",true,time()+14400,"/");
                       redirect::reload();
                   }else{
                       message::set("Ошибка логина или пароля");
                   }
               }else{
                   message::set("Ошибка логина или пароля");
               }
            }
        }

        //return $this->twig()->render("front/cabinet/login.twig");
    }


    public function favorites()
    {
        return $this->twig()->render("front/favorites.twig");
    }
}