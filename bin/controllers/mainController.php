<?php
/**
 * Created by PhpStorm.
 * User: $WebDev
 * Date: 14.05.2020
 * Time: 3:14
 */

namespace bin\controllers;

use \bin\main\container;

class mainController extends container
{
    public function index()
    {
        return $this->twig()->render("front/home.twig");
    }
}